# FancyCalculator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.5. <br>
<br>
This app was created by Yoga Bhiantara 2018;

Instalation:

0. You must first clone service backend from https://gitlab.com/bhiantara/backend-restapi-fancy-calculator.git
1. Clone Fancy-Calculator APP from this repo
2. Open project and "npm install" module
3. Start service with command "ng serve"
4. Fancy calculator is running on localhost:4200

Getting something wrong?
you can contact me: gusss.yoga@gmail.com <br>
Demo Screenshot mobile view<br>
<img src="https://gitlab.com/bhiantara/screenshotapp/raw/master/fancy-calculator/ss1-fancycalcultor-mobile.jpg">
<img src="https://gitlab.com/bhiantara/screenshotapp/raw/master/fancy-calculator/ss4-fancycalcultor-mobile.jpg">
<img src="https://gitlab.com/bhiantara/screenshotapp/raw/master/fancy-calculator/ss2-fancycalcultor-mobile.jpg">
<img src="https://gitlab.com/bhiantara/screenshotapp/raw/master/fancy-calculator/ss3-fancycalcultor-mobile.jpg">
<br>
<br>
Demo Screenshot desktop view<br>
<img src="https://gitlab.com/bhiantara/screenshotapp/raw/master/fancy-calculator/ss1-fancycalcultor-desktop.jpg"><br>
<img src="https://gitlab.com/bhiantara/screenshotapp/raw/master/fancy-calculator/ss2-fancycalcultor-desktop.jpg"><br>
<img src="https://gitlab.com/bhiantara/screenshotapp/raw/master/fancy-calculator/ss3-fancycalcultor-desktop.jpg">
