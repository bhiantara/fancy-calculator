import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {CalculatorComponent} from './calculator/calculator.component';
import {appRoutes} from './_config/router.config';
import {HomeComponent} from './home.component';
import {MaterialModule} from './_shared-module/material.module';
import { HistoryComponent } from './history/history.component';
import {CalculatorService} from './_services/calculator.service';
import {EndpointService} from './_services/endpoint.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        CalculatorComponent,
        HistoryComponent
    ],
    imports: [
        BrowserModule,
        MaterialModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [EndpointService, CalculatorService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
