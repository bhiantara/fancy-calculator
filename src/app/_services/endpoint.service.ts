import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class EndpointService {
    private api = 'http://localhost:8080/';

    constructor(private http: HttpClient) {
    }

    getHeader(): Object {
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json');
        return {headers: headers};
    }

    get(url: string): Observable<any> {
        return this.http.get(this.api + url, this.getHeader());
    }

    post(url: string, post: Object): Observable<any> {
        return this.http.post(
            this.api + url, JSON.stringify(post),
            this.getHeader()
        );
    }

    put(url: string, post: Object): Observable<any> {
        return this.http.put(this.api + url, JSON.stringify(post), this.getHeader());
    }
}

