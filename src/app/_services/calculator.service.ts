import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {EndpointService} from './endpoint.service';
import {map} from 'rxjs/operators';

@Injectable()
export class CalculatorService {
    constructor(private _endpoint: EndpointService) {
    }

    get(): Observable<any> {
        return this._endpoint.get('history').pipe(map(
            res => {
                return res;
            }
        ));
    }

    post(data): Observable<any> {
        return this._endpoint.post('add_history', data).pipe(map(
            res => {
                return res;
            }
        ));
    }

    delete(data): Observable<any> {
        return this._endpoint.post('delete_history', data).pipe(map(
            res => {
                return res;
            }
        ));
    }
}
