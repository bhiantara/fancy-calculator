import {Component, OnInit} from '@angular/core';
import {CalculatorService} from '../_services/calculator.service';
import {MatSnackBar} from '@angular/material';
import {switchMap} from 'rxjs/internal/operators';

@Component({
    selector: 'app-calculator',
    templateUrl: './calculator.component.html',
    styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
    title = 'fancy-calculator';
    input = '0';
    final = false; // detect reset if press '='
    $history = false;
    datahistory;
    service = false;

    constructor(private _calculator: CalculatorService,
                private _snack: MatSnackBar) {
    }

    ngOnInit() {
        this._calculator.get().subscribe(
            res => {
                this.datahistory = res.data;
                this.service = true;
            }, error => {
                this._snack.open('Service not running. Please check service!', '', {duration: 10000});
            }
        );
    }

    addChar(character) {
        this.input = this.final ? '' : this.input;
        this.input = this.input === '0' ? character : this.input + character;
        this.final = false;
        if (this.input.charAt(this.input.length - 2) === '^') {
            const hasil = Math.pow(parseInt(this.input.slice(0, this.input.length - 2)), parseInt(character)).toString();
            const data = {hitung: this.input, hasil: hasil};
            this.input = hasil;
            this.postdata(data);
            this.final = true;
        }
    }

    cos() {
        const hasil = Math.cos(eval(this.input)).toString();
        const data = {hitung: this.input, hasil: hasil, spesial: 'cos'};
        this.input = hasil;
        this.postdata(data);
        this.final = true;
    }

    sin() {
        const hasil = Math.sin(eval(this.input)).toString();
        const data = {hitung: this.input, hasil: hasil, spesial: 'sin'};
        this.input = hasil;
        this.postdata(data);
        this.final = true;
    }

    tan() {
        const hasil = Math.tan(eval(this.input)).toString();
        const data = {hitung: this.input, hasil: hasil, spesial: 'tan'};
        this.input = hasil;
        this.postdata(data);
        this.final = true;
    }

    sqrt() {
        const hasil = Math.sqrt(eval(this.input)).toString();
        const data = {hitung: this.input, hasil: hasil, spesial: 'sqrt'};
        this.input = hasil;
        this.postdata(data);
        this.final = true;
    }

    square() {
        this.input = eval(this.input + '*' + this.input).toString();
    }

    lg() {
        const hasil = Math.log(eval(this.input)).toString();
        const data = {hitung: this.input, hasil: hasil, spesial: 'log'};
        this.input = hasil;
        this.postdata(data);
        this.final = true;
    }

    exp() {
        this.input += '^';
    }

    deleteChar() {
        this.input = this.input.substring(0, this.input.length - 1);
        if (this.input === '') {
            this.input = '0';
        }
    }

    changeSign() {
        if (this.input.substring(0, 1) === '-') {
            this.input = this.input.substring(1, this.input.length);
        } else {
            this.input = '-' + this.input;
        }
    }

    compute() {
        console.log('hasil', this.input, eval(this.input));
        const hasil = eval(this.input).toString();
        const data = {hitung: this.input, hasil: hasil};
        this.input = hasil;
        this.postdata(data);
        this.final = true;
    }

    postdata(data) {
        this._calculator.post(data).pipe(
            switchMap(res => {
                return this._calculator.get();
            })
        ).subscribe(res => {
            this.datahistory = res.data;
        });
    }

    history() {
        this.$history = !this.$history;
    }

    edit(data) {
        this.input = data.hitung;
    }

    delete(data) {
        this._calculator.delete({id_history: data.id_history}).pipe(
            switchMap(res => {
                return this._calculator.get();
            })).subscribe(
            res => {
                this.datahistory = res.data;
            }
        );
    }

    hasil(data) {
        this.input = data;
    }

}
