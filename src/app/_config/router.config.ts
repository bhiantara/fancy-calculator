import { Routes, CanActivate } from '@angular/router';
import { HomeComponent } from '../home.component';
import {CalculatorComponent} from '../calculator/calculator.component';
import {HistoryComponent} from '../history/history.component';

const homeRoutes: Routes = [
    { path: 'calculator', component: CalculatorComponent},
    { path: 'history', component: HistoryComponent}
];

export const appRoutes: Routes = [
    { path: '', component: HomeComponent, children: homeRoutes}
];
