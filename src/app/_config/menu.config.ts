export const menu: Array<Object> = [
      {icon: 'book', path: '/calculator', name: 'Fancy Calculator'},
      {icon: 'bookmarks', path: '/history', name: 'History Calculator'},
    ];
