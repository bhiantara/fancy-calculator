import {NgModule} from '@angular/core';
import localeId from '@angular/common/locales/id';
import {
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatDatepickerModule,
    DateAdapter,
    MAT_DATE_FORMATS,
    NativeDateAdapter,
    MatDateFormats,
    MatSelectModule,
    MatTabsModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule
} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CommonModule, DatePipe, DecimalPipe, registerLocaleData} from '@angular/common';

const MY_NATIVE_DATE_FORMATS: MatDateFormats = {
    parse: {
        dateInput: {year: 'numeric', month: 'numeric', day: 'numeric'}
    },
    display: {
        dateInput: {year: 'numeric', month: 'long', day: 'numeric'},
        monthYearLabel: {year: 'numeric', month: 'short'},
        dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
        monthYearA11yLabel: {year: 'numeric', month: 'long'},
    }
};

registerLocaleData(localeId);

@NgModule({
    imports: [
        MatButtonModule,
        MatNativeDateModule,
        MatSnackBarModule,
        MatIconModule,
        MatCardModule,
        MatInputModule,
        MatMenuModule,
        MatSelectModule,
        MatDialogModule,
        MatButtonToggleModule,
        MatDatepickerModule,
        MatSelectModule,
        MatPaginatorModule,
        MatSortModule,
        MatTooltipModule,
        MatTableModule,
        MatTabsModule,
        MatCheckboxModule,
        MatProgressBarModule,
        FlexLayoutModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatRadioModule,
        CommonModule,
        MatAutocompleteModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        CdkTableModule
    ],
    exports: [
        MatButtonModule,
        MatSnackBarModule,
        MatIconModule,
        MatCardModule,
        MatInputModule,
        MatMenuModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
        MatDialogModule,
        MatCheckboxModule,
        MatTooltipModule,
        MatSelectModule,
        MatButtonToggleModule,
        MatDatepickerModule,
        FlexLayoutModule,
        MatSelectModule,
        MatProgressBarModule,
        MatTabsModule,
        MatProgressBarModule,
        MatSelectModule,
        MatTabsModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatRadioModule,
        MatAutocompleteModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        CdkTableModule
    ],
    providers: [
        DatePipe, DecimalPipe,
        {provide: DateAdapter, useClass: NativeDateAdapter},
        {provide: MAT_DATE_FORMATS, useValue: MY_NATIVE_DATE_FORMATS}
    ]
})
export class MaterialModule {
}
