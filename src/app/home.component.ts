import { Component, OnInit, Inject} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import {MediaChange, ObservableMedia} from '@angular/flex-layout';
import { menu } from './_config/menu.config';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  menu = menu;
  private sizemedia$;
  isOpen = true;
  sidemode = 'side';
  backdrop = false;
  appTitle = 'Fancy Calculator APP';
  headerLoading: boolean;
  tutor = true;
  pageToolbar;
  home = true;

  constructor(
    // private loader: LoaderService,
    public router: Router,
    private media$: ObservableMedia,
  ) {
    this.pageToolbar = {
      icon: 'home',
      name: 'Fancy Calcultor'
    };

    this.sizemedia$ = media$.subscribe((change: MediaChange) => {
      const isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
      if (isMobile) {
        this.sidemode = 'over';
      } else {
        this.sidemode = 'side';
      }
    });
  }

  ngOnInit() {
  }

  setToolbar(item) {
    this.pageToolbar.name = item.name;
    this.pageToolbar.icon = item.icon;
  }

  closeTutor() {
    this.tutor = false;
  }

}
