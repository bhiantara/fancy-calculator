import { Component, OnInit } from '@angular/core';
import {CalculatorService} from '../_services/calculator.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'fancy-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  datahistory;
  service = false;
  constructor(private _calculator: CalculatorService, private _snack: MatSnackBar) { }

    ngOnInit() {
        this._calculator.get().subscribe(
            res => {
                this.datahistory = res.data;
                this.service = true;
            }, error => {
                this._snack.open('Service not running. Please check service!', '', {duration: 5000});
            }
        );
    }

}
